package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods {
	
	public void MergeLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="(//img[@src='/images/fieldlookup.gif'])[1]")
	WebElement eleFromLead_LookUp;
	
	public MergeLeadsPage clickFromLead_LookUp() {
		click(eleFromLead_LookUp);
		return this;
	}
	
	@FindBy(xpath="(//img[@src='/images/fieldlookup.gif'])[2]")
	WebElement eleToLead_LookUp;
	
	public MergeLeadsPage clickToLead_LookUp() {
		click(eleToLead_LookUp);
		return this;
	}
	
	public FindLeads_LookUp_Page switchWindowTo_FindLeadsLookUp(Integer index) {
		switchToWindow(index);
		return new FindLeads_LookUp_Page();
	}
	
	@FindBy(linkText="Merge") WebElement eleMergeLeads;
	public MergeLeadsPage clickMergeLeads() {
		click(eleMergeLeads);
		return this;
	}
	
	public MergeLeadsPage acceptAlertMergeLeads() {
		acceptAlert();
		return this;
	}
}
