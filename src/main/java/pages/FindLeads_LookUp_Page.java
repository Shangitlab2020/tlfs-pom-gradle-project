package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads_LookUp_Page extends ProjectMethods{
	
	public FindLeads_LookUp_Page() {
		PageFactory.initElements(driver, this);
	}
	

//	@FindBy(xpath="//input[@name='id']")
//	WebElement eleLeadidField;
//
//	public FindLeads_LookUp_Page enterLeadidField(String data) {
//		type(eleLeadidField, data);
//		return this;
//	}
	
	@FindBy(how = How.XPATH,using="//input[@name='id']")
	WebElement eleLeadId;
	public FindLeads_LookUp_Page enterLeadId(String data) {
		type(eleLeadId, data);
		return this;
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[1]")
	WebElement elesearchedByLeadId;
	
	public MergeLeadsPage searchedLeadId() {
		click(elesearchedByLeadId);
		return new MergeLeadsPage();
	}
	
	
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement eleFindLeadButton;
	
	public FindLeads_LookUp_Page clickFindLead() {
		click(eleFindLeadButton);
		return this;
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[1]") WebElement eleLeadRow;
	public FindLeads_LookUp_Page selectLeadRow(){
		click(eleLeadRow);
		return this;
	}
	
	public MergeLeadsPage switchWindowTo_MergeLeadsPage(Integer index) {
		switchToWindow(index);
		return new MergeLeadsPage();
	}

}
