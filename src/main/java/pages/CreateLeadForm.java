package pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadForm extends ProjectMethods {
	
	public CreateLeadForm() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using = "createLeadForm_companyName")
	WebElement eleCompName;
	
	public CreateLeadForm companyNameField(String data) {
		type(eleCompName,data);
		return this;
	}
	
	@FindBy(how = How.ID,using = "createLeadForm_firstName")
	WebElement eleFirstName;
	
	public CreateLeadForm firstNameField(String data) {
		type(eleFirstName,data);
		return this;
	}

	
	@FindBy(how = How.ID,using = "createLeadForm_lastName")
	WebElement eleLastName;
	
	public CreateLeadForm LastNameField(String data) {
		type(eleLastName,data);
		return this;
	}
	
	@FindBy(how = How.CLASS_NAME, using="smallSubmit")
	WebElement eleSubmitButton;
	
	public ViewLeadPage submitButton() {
		click(eleSubmitButton);
		return new ViewLeadPage();
	}
	
}
