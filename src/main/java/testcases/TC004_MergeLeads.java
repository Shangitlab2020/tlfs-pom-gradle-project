package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_MergeLeads extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_MergeLead";
		testDescription = "MergeLead";
		authors = "Shankar Matheswaran";
		category = "smoke";
		dataSheetName = "TC004";
		testNodes = "MergeLead";
	}
	
	@Test(dataProvider="fetchData")
	public void mergeLeads(String userName, String password, Integer index, String leadId, Integer index1, Integer index2, 
			String leadId1, Integer index3) {
		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfm()
		.clickLeads()
//		.mergeLeadsLink()
//		.clickFromLead_LookUp()
//		.switchWindowTo_FindLeadsLookUp(index)
//		.enterLeadId(leadId)
//		.clickFindLead()
//		.selectLeadRow()
//		.switchWindowTo_MergeLeadsPage(index1)
//		.clickToLead_LookUp()
//		.switchWindowTo_FindLeadsLookUp(index2)		
//		.enterLeadId(leadId1)
//		.clickFindLead()
//		.switchWindowTo_MergeLeadsPage(index3)
//		.clickMergeLeads()
//		.acceptAlertMergeLeads()
		;
		
		
		
		
		
		
	}

}
