package testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreatLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLead";
		authors = "Shankar Matheswaran";
		category = "smoke";
		dataSheetName = "TC002";
		testNodes = "CreateLeads";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String userName, String password, String CName, String FName, String LName ) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfm()
		.clickLeads()
		.createLeadLink()
		.companyNameField(CName)
		.firstNameField(FName)
		.LastNameField(LName)
		.submitButton();		
		
	}

}
