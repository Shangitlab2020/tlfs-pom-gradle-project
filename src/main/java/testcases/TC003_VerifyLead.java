package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_VerifyLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_VerifyLead";
		testDescription = "To Verify the Lead's First Name";
		authors = "Shankar Matheswaran";
		category = "Smoke";
		dataSheetName = "TC003";
		testNodes = "VerifyLeads";
	}
	
	@Test(dataProvider="fetchData")
	public void verifyLead(String userName, String password, String CName, String FName, String LName ) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfm()
		.clickLeads()
		.createLeadLink()
		.companyNameField(CName)
		.firstNameField(FName)
		.LastNameField(LName)
		.submitButton()
		.verifyFName(FName)
		;
	}



}
